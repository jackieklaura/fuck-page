+++
title = "frequently asked questions"
hideReadMore = true
+++
# für wen ist fuck?
wir sind frauen, non-binary und trans personen und mögen computer kram. wenn das auch auf dich zutrifft, bist du hier richtig!

# darf ich kommen, wenn ich noch gar nicht programmieren kann?
ja!!!

# muss ich denn schon was können um bei euch vorbeizuschauen?
nein! du musst weder programmieren können, noch ein linux stystem am laufen
haben oder mit der kommandozeile vertraut sein. wir wollen gemeinsam einen raum schaffen,
in dem du dir das aneignen kannst, wenn du möchtest.

# habt ihr einen coc?
wir wollen uns einen code of conduct geben und wir arbeiten dran. uns ist wichtig, dass rassismus, transfeindlichkeit, misogynie, ableismus usw keinen platz in unserer community haben. wir wollen eine kultur schaffen in der wir aktiv daran arbeiten, dass das auch so gelebt wird. ein coc darf für uns deshalb kein reines lippenbekenntnis sein. es muss auch klar sein was passiert, wenn was passiert und wie. weil das keine triviale frage ist, ist er noch nicht fertig. du bist herzlich eingeladen mitzuwirken, wenn du möchtest!

# muss ich für ein online treffen zoom oder google meets oder sonst was installiert haben?
nein. die treffen finden über jitsi statt.
- am computer
  du kannst den link zum meeting einfach im browser öffnen ohne was zu installieren
  (es kann sein, das nicht alle browser gleich gut funktionieren und wenn du schwierigkeiten hast
  probier es vielleicht über einen anderen browser)

- am handy:
  am handy brauchst du die jitsi app um dich verbinden zu können
  [fdroid](https://f-droid.org/de/packages/org.jitsi.meet/),
  [android-store](https://play.google.com/store/apps/details?id=org.jitsi.meet&gl=DE)
  [apple-store](https://apps.apple.com/de/app/jitsi-meet/id1165103905)

# was passiert während den meetings?
schau bei den [terminen](/termine/) nach und klicke auf mehr informationen. dort findest du eine beschreibung zu den terminen.

# wie bekomme ich den link zu einem treffen?
den link für die veranstaltungen gibts im [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems), [twitter dm](https://twitter.com/_fuckthesystems) oder  [mastodon dm](https://chaos.social/@fuckthesystems)

# ich bin schon dabei und würde gerne eine person mitbringen
bitte poste die links zu den veranstaltungen nirgends öffentlich, aber du kannst den link gerne an einzelpersonen weiterleiten

# ich würde gerne mitmachen, mag aber keine video calls
es gibt auch einen matrix space in dem du auch ganz ohne videocalls bei fuck mitmachen kannst.
ping uns einfach über [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems), [twitter dm](https://twitter.com/_fuckthesystems) oder  [mastodon dm](https://chaos.social/@fuckthesystems)

# wie komme ich in den matrix space?
in den matrix space kannst du eingeladen werden, wenn du bei einem unserer online treffen warst oder wenn du eine person kennst, die schon drin ist. wenn du schon einen account hast kannst du uns einfach deinen account namen sagen und du bekommst eine einladung.
wenn du noch keinen hast gibt es [hier](/matrix/) eine anleitung, wie du dich anmelden kannst.

