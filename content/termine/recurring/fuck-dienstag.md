+++
title = "fuck dienstag - dinge tun"
summary = "jeden 1. und 3. dienstag im monat ab 19:00 auf deutsch"
readmore = "weitere infos..."
weight = 30
+++

# wann?
am 1. und 3. dienstag im monat um 19:00

# wo?
auf jitsi (link gibts im [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems), [twitter dm](https://twitter.com/_fuckthesystems) oder  [mastodon dm](https://chaos.social/@fuckthesystems))

# was?
die dienstags treffen sind dafür gedacht gemeinsam (oder alleine) an projekten zu arbeiten. um 19:00 gibt es einen kurzen call wo wir uns erzählen, woran wir arbeiten und dann teilt sich das in kleinere gruppen auf, oder leute verlassen das jitsi wider um in ruhe an ihrem projekt zu arbeiten.
ihr könnt schon fertige projektideen mitbringen. wenn ihr gerne coden anfangen wollt, aber noch nicht so recht wisst, wo könnt ihr auch vorbeikommen und euch tipps und links zu tutorials holen.

# sprache: deutsch