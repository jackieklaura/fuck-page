+++
title = "fuck thursday"
summary = "every 2nd and 4th thursday of the month at 8pm CEST in english"
weight = 20
+++

# when?

the second and fourth thursday of the month (alternating with [fuck donnerstag](/termine/recurring/fuck-donnerstag/))  
8 pm (CEST) until noone is there anymore

# where?
online via jitsi (get the link in [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), via [email](mailto:hello@fuck-the.systems), [twitter dm](https://twitter.com/_fuckthesystems) oder  [mastodon dm](https://chaos.social/@fuckthesystems))

# what?
the thursday meeting is the ideal place to come and say hi and meet fuck network.
there is an introduction round and a short introduction to fuck.
the meeting is a nice babble hangout where topics vary from week to week depending on what the participants want to talk about. it is possible to split up in smaller groups depending on topic wishes and groupsize

# language: english
