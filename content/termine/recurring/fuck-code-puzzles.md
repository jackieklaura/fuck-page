+++
title = "fuck code puzzles"
summary = "jeden donnerstag von 19:00 - 20:00 uhr"
readmore = "weitere infos..."
weight = 40
+++

# wann?
jeden donnerstag von 19:00 - 20:00 uhr (vor fuck thursday oder fuck donnerstag)

# wo?
auf jitsi (link gibts im [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems), [twitter dm](https://twitter.com/_fuckthesystems) oder  [mastodon dm](https://chaos.social/@fuckthesystems))

# was?
vor dem donnerstags meeting treffen wir uns und versuchen gemeinsam an einem coding puzzle zu tüfteln. ihr könnt gerne puzzles mitbringen, ansonsten wird es vermutlich ein coding puzzle aus dem internet.
wenn wir mehr als 3 leute sind werden wir uns in kleinere gruppen aufteilen. die sprache mit der wir das lösen ergibt sich aus den anwesenden in der gruppe.
auch wenn ihr noch gar nicht wisst wie anfangen, seid ihr herzlich willkommen und dürft immer gerne mittüfteln

# sprache
deutsch oder englisch je nachdem wer in der gruppe ist