+++
title = "[matrix] - how to"
hideReadMore = true
+++

# was ist [matrix]?
[matrix] ist ein chatprotokoll so wie smtp, irc oder xmpp. element ist der populärste client für [matrix]. du kannst auch andere clients verwenden aber es kann dann sein, dass manches nicht so funktioniert wie für die meisten (zum beispiel ende zu ende verschlüsselung). [element](https://element.io/get-started) gibt es als browser app, für den desktop(linux, mac, windows), android und iphone.

wir haben unseren eigenen server aber du musst keinen account auf unserem server haben, um in unseren space zu kommen.

# was ist ein space?
damit nicht unsere ganze kommunikation in einem einzigen raum stattfindet haben wir einen f.u.c.k. space in dem verschiedene räume zu unterschiedlichen themen gebündelt sind (das ist so wie workspaces bei slack). wenn du in unseren space kommst kannst du alle räume sehen und wenn du möchtest auch joinen.

# wie kann ich mich anmelden?
wenn du noch keinen account auf unserem server hast, kannst du von uns einen token bekommen. mit dem token, kannst du einen account registrieren. die screenshots sind von der browser app.

# was mach ich mit dem token?
du hast von uns einen link bekommen mit dem du dich anmelden kannst. der token ist da schon eingetragen oder du kannst ihn im token feld eintragen. such dir einen username aus uns benutz ein sicheres passwort.


![fuck-the.systems registration](/element/00_register.png)

# einloggen
du kommst dann zum sign in screen, die folgenden schritte kannst du auch in deiner bevorzugten app erledigen.


![fuck-the.systems signin](/element/01_signin.png)

# der richtige homeserver
beim sign in form musst du darauf achten, dass der richtige homeserver eingetragen ist. Das ist vor allem dann wichtig, wenn du eine app benutzt in der nicht automatisch der richtige homeserver also ```fuck-the.systems``` gesetzt ist. meistens steht da ```matrix.org```, der standardserver von [matrix].


![fuck-the.systems signin form](/element/02_signin_server.png)

# einladung zum space
wenn du dich eingeloggt hast brauchst du noch die einladung zum space, das kann jede person machen, die schon in dem space ist. dann erscheint auf der linken seite ein feld mit unserem logo und einem roten ausrufezeichen. da klickst du drauf und solltest dann die einladung annehmen können. falls das nicht sofort klappt frag am besten eine*n von uns, damit wir das gemeinsam hinkriegen (manchmal ist das etwas langsam oder klappt nicht beim ersten mal. das space feature ist noch nicht so alt und manchmal noch etwas unzuverlässig.)


![fuck space invite](/element/04_invite_fuck.png)

# die raumübersicht
als erstes solltest du nun die raumübersicht von unserem space sehen. du kannst alle diese räume joinen, für den anfang sind vor allem ```main``` und ```welcome``` spannend. ```main``` ist unser zentraler channel, um infos zu teilen, zu plaudern, events anzukündigen usw. die anderen räume sind zu bestimmten themen.


![übersicht der räume](/element/06_room_overview.png)


du kannst immer zur raumübersicht zurück indem du auf das space logo auf der linken seite klickst.


![zurück zur raumübersicht](/element/09_back_to_overview.png)


# hurra du hast es geschafft! happy chatting! <3
alles was jetzt noch kommt ist optional bzw. wird vielleicht später für dich relevant

# secure backup?
ziemlich bald wird oben auf der linken seite ein dialog angezeigt, der dich fragt ob du einen secure backup machen magst. den solltest du speichern, damit du in zukunft auch verschlüsselte nachrichten lesen kannst, wenn du dich ausgeloggt hattest oder ein anderes device verwenden möchtest. speicher den code am besten in einem passwortmanager ab oder an einem anderen ort, wo du ihn wiederfindest aber am besten so, dass er (für alle, die nicht du sind) nicht zu einfach zu finden ist.


![secure backup](/element/07_secure_backup.png)


element verschlüsselt kommunikation ende zu ende, das heißt du brauchst immer einen gültigen schlüssel, um die nachrichten in einem verschlüsselten raum zu lesen. falls du dich von einem neuen device oder mit einem anderen browser einloggst können die alten nachrichten also nicht gelesen werden. bei einem login von einem neuen device musst du also erst einmal beweisen, dass du berechtigt bist, die nachrichten zu entschlüsseln. es erscheint wieder oben links ein dialog und dann hast du zwei möglichkeiten.


![verify device](/element/10_verify_device.png)


verify with security key bedeutet, dass du den code, den du gespeichert hattest eingibst. das funktioniert immer, egal ob du noch in einer anderen session eingeloggt bist.

alternativ kannst du deine devices/sessions gegenseitig verifizieren. du musst dazu zugang zu beiden sessions gleichzeitig haben, also zum beispiel handy und desktop. wenn du mit einem device eine verifizierung startest kommt auf dem anderen gerät eine verifizierungsanfrage.


![verifizierungsanfrage](/element/13_other_device.png)


du nimmst die anfrage an und du vergleichst, ob bei beiden devices die gleichen emojis angezeigt werden. wnen das so ist, musst du nur noch bestätigen und ab sofort sollten beide devices in der lage sein, die gleichen verschlüsselten nachrichten zu lesen. nach dem gleichen prinzip funktioniert übrigens auch die verifizierung mit anderen user*innen.

![vergleiche emojis](/element/15_emoji_compare.png)


# probleme beim einrichten?
wir haben einen [channel](https://element.fuck-the.systems/#/room/#support:fuck-the.systems) für troubleshooting, ansonsten kannst du uns auf unseren anderen kanälen anpingen, falls etwas nicht klappt. 